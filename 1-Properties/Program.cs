﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{

    enum ItalianSeeds
    {
        Denari,
        Coppe,
        Spade,
        Bastoni
    }


    enum ItalianNames
    {
        Asso,
        Due,
        Tre,
        Quattro,
        Cinque,
        Sei,
        Sette,
        Fante,
        Cavallo,
        Re
    }

    class Program
    {
        private static void Main(string[] args)
        {
            var df = new DeckFactory
            {
                Names = Enum.GetNames(typeof(ItalianNames)).ToList(),
                Seeds = Enum.GetNames(typeof(ItalianSeeds)).ToList()
            };
            
            Console.WriteLine("The {1} deck has {0} cards: ", df.GetDeckSize(), "italian");

            foreach (var c in df.GetDeck())
            {
                Console.WriteLine(c);
            }

            Console.ReadLine();
        }
    }
}
