﻿using System;

namespace Properties {

    public class Card
    {
        public string Seed { get; }
        public string Name { get; }
        public int Ordinal { get; }

        public Card(string name, string seed, int ordinal)
        {
            Name = name;
            Ordinal = ordinal;
            Seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        public override string ToString()
        {
            return $"{this.GetType().Name}(Name={Name}, Seed={Seed}, Ordinal={Ordinal})";
        }

        public override bool Equals(object obj)
        {
            var other = (Card)(obj ?? new Card("","",0));
            return Seed.Equals(other.Seed) &&
                Name.Equals(other.Name) &&
                Ordinal.Equals(other.Ordinal);
        }

        public override int GetHashCode()
        {
            var hashCode = (Seed != null ? Seed.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ Ordinal;
            return hashCode;
        }
    }

}