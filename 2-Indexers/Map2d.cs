﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        private readonly Dictionary<Tuple<TKey1, TKey2>, TValue> _dictionary;

        public Map2D()
        {
            _dictionary = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }
        
        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            if (other == null) throw new ArgumentNullException(nameof(other));
            var myElements = GetElements();
            var otherElements = other.GetElements();
            return myElements.Equals(otherElements);
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get => _dictionary[new Tuple<TKey1, TKey2>(key1,key2)];
            set => _dictionary[new Tuple<TKey1, TKey2>(key1,key2)] = value;
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            return _dictionary.Where(val => val.Key.Item1.Equals(key1))
                .Select(val => new Tuple<TKey2, TValue>(
                    val.Key.Item2, val.Value))
                .ToList();
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            return _dictionary.Where(val => val.Key.Item2.Equals(key2))
                .Select(val => new Tuple<TKey1, TValue>(
                    val.Key.Item1, val.Value))
                .ToList();
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            return _dictionary.Select(val => 
                new Tuple<TKey1, TKey2, TValue>(
                    val.Key.Item1, val.Key.Item2, val.Value))
                .ToList();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            if (keys1 == null) throw new ArgumentNullException(nameof(keys1));
            if (keys2 == null) throw new ArgumentNullException(nameof(keys2));
            if (generator == null) throw new ArgumentNullException(nameof(generator));
            var keys2List = keys2.ToList();
            foreach (var key1 in keys1)
            {
                foreach (var key2 in keys2List)
                {
                    _dictionary.Add(new Tuple<TKey1, TKey2>(key1,key2), generator(key1,key2));
                }
            }
        }

        public int NumberOfElements => _dictionary.Count;

        public override string ToString()
        {
            var tmp = GetElements().Aggregate("", (current, val) => 
                current + $"[{val.Item1},{val.Item2},{val.Item3}], ");
            tmp = tmp.Remove(tmp.LastIndexOf(','));
            return $"{this.GetType().Name} {{{tmp}}}";
        }
    }
}
